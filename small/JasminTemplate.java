/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import java.util.ArrayList;

/**
 * Created by simon on 08/06/17.
 */
public class JasminTemplate  {

    public static String[] theText = new String[]{
            ".class public (~CLASSNAME~)\n\n",
            ".super java/lang/Object\n",
            ".field private static scanner$ Ljava/util/Scanner;\n",

            ".method public <init>()V\n",
            ".limit stack 10\n",
            "aload_0\n",
            "invokespecial java/lang/Object/<init>()V\n",
            "return\n",
            ".end method\n",

            ".method private static init$statics()V\n",
            ".limit stack 4\n",
            "new java/util/Scanner\n",
            "dup\n",
            "getstatic java/lang/System/in Ljava/io/InputStream;\n",
            "invokespecial java/util/Scanner/<init>(Ljava/io/InputStream;)V\n",
            "putstatic (~CLASSNAME~)/scanner$ Ljava/util/Scanner;\n",
            "return\n",
            ".end method\n",

            ".method private static read$int()I\n",
            ".limit stack 4\n",
            "getstatic (~CLASSNAME~)/scanner$ Ljava/util/Scanner;\n",
            "invokevirtual java/util/Scanner/nextInt()I\n",
            "ireturn\n",
            ".end method\n",

            ".method private static print$it(I)V\n",
            ".limit stack 4\n",
            "getstatic java/lang/System/out Ljava/io/PrintStream;\n",
            "iload_0\n",
            "invokevirtual java/io/PrintStream/print(I)V\n",
            "return\n",
            ".end method\n",

            ".method private static print$it(Ljava/lang/String;)V\n",
            ".limit stack 4\n",
            "getstatic java/lang/System/out Ljava/io/PrintStream;\n",
            "aload_0\n",
            "invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n",
            "return\n",
            ".end method\n",


            ".method public static main([Ljava/lang/String;)V\n",
            ".limit stack 10\n",
            "invokestatic (~CLASSNAME~)/init$statics()V\n",
            "(~CODE~)\n",
            "return\n",
            ".limit locals (~LOCALS~)\n",
            ".end method\n",
            "\n"
    };



}
