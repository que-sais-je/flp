/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import java.util.Stack;

/** LoopScope keeps track of where to branch to if 'continue' or 'break' are used.
 * Current neither is implemented.
 *
 * {@link #nextLabel()} and {@link #nextLabel(String)} can also be used whenever a unique name is needed.
 *
 */
public class LoopScope {

    private static int nextLabel = 0;

    /** Return a unique name based on the input parameter.
     *
     * @param base any string which could be a legal Jasmin name.
     * @return A new unique name.
     */
    public static String nextLabel(String base) {
        return String.format("%s$%d$", base, nextLabel++);
    }

    /** Return a unique name. Equivalent to {@code nextLabel("label$")}
     *
     * @return A new unique name.
     */
    public static String nextLabel() {
        return nextLabel("label$");

    }

    static private class LoopList {
        public String   block;
        public String[] label;

        public LoopList(String blockName, String... labels) {
            this.block      = blockName;
            int labelLength = labels.length;
            this.label      = new String[labelLength];
            for(int i = 0; i < labelLength; i++) {
                this.label[i] = nextLabel(labels[i]);
            }
        }
    }

    final private static String LoopBlock = "-- loop --";

    final private static Stack<LoopList>  scope = new Stack();

    /** End of a block of code.  The labels used since {@link #begin(String...)} or {@link #beginNamed(String, String...)} are discarded.
     *
     */
    public static void end() {
        scope.pop();
    }

    /**
     * Create a named block allowing multilevel break and continue
     * @param blockName name to use for the block.
     * @param labels names used to invent unique labels.
     */
    public static void beginNamed(String blockName, String... labels) {
        LoopList newL = null;
        if(labels.length == 0) {
            newL = new LoopList(blockName,
                                nextLabel("continue"),
                                nextLabel("break")
                                );
        } else {
            newL = new LoopList(blockName, labels);
        }
        scope.push(newL);
    }

    /**
     * Create a unnamed block allowing single level break and continue
     * @param labels names used to invent unique labels.
     */
    public static void begin(String... labels) {
        scope.push(new LoopList(LoopBlock, labels));
    }

    /**
     * Get the nth label from the current block
     * @param n label number.  By default n=0 has "continue" as its base name; n=1 has "break"
     * @return label to jump to.
     */
    public static String  label(int n) {
        return scope.peek().label[n];
    }

    /**
     * Find a named block.  Return null if not found.
     * @param name name of block to search for.
     * @param n index of the lable to return.
     * @return null if the block isn't found, otherwise the requested label from that block.
     */
    private static String label(String name, int n) {
        int p = scope.size() - 1;
        while(p >= 0) {
            LoopList lp = scope.get(p);
            if(name.equals(lp.block)) return lp.label[n];
            --p;
        }
        return null;
    }



}
