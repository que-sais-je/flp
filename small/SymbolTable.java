/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by simon on 31/05/17.
 */
public class SymbolTable extends HashMap<String, Integer> {

    private int nextLocal;



    public SymbolTable(int n) {
        super();
        this.nextLocal = n;
    }

    public SymbolTable() {
        this(0);
    }



    public int locals() {
        return nextLocal;
    }


    public int getToLoad(String name) {

        return this.getOrDefault(name, -1);
    }

    public int getToStore(String name) {
        Integer m = this.getOrDefault(name, -1);
        if (m < 0) {
            m = this.nextLocal++;
            this.put(name, m);
        }
        return m;
    }

}
