/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

/**
 * Created by simon on 26/05/17.
 */

import java.io.PrintStream;
import java.io.PrintWriter;

/** Small routines for outputting to a PrintStream (by default {@link System#out} ).
 *
 * CoderWrite handles only a few commands - setting labels, emitting opcodes etc. It hides as much
 * as possible of the error prone assembler language structure.

 */
public class CodeWriter {

private static PrintStream out = System.out;

    /**
     * Specify the print stream to use.
     * @param outStream The stream to use (default is {@link System#out})
     */
    public static void output(PrintStream outStream) { out = outStream; }

    public static PrintStream output() { return  out; }


    /** Calls printf on the {@link PrintStream}
     *
     * @param format format string - as for usual printf calls.
     * @param rest  arguments to format string.
     */
    public static void emitf(String format, Object... rest) {
        out.printf(format, rest);
    }

private static String indent = "    ";

public static void indent() {
   indent += "  ";
}

public static void outdent() {
    indent = indent.substring(2);
}

    /**
     * Output a line of assembler for an operation (such as nop, swap, etc) without arguments.
     * @param code - name of the operation.
     *
     *  Example {@code emit("swap");}
     */
    public static void emit(String code) {
        emitf(indent+"%s\n", code);
    }

    /**
     * Output a line of assembler for an operation (such as load, store, etc) with a single argument.
     * @param code  name of the operation.
     * @param operand  operand used by code.
     *
     * Example {@code emit("new", "java/util/Scanner");}
     */
    public static void emit(String code, String operand) {
        emitf(indent+"%s %s\n", code, operand);
}


    /**
     * As for {@link CodeWriter#emit(String, String)} but with an integer argument.
     * @param code  name of the operation.
     * @param operand   operand used by code.
     */
    public static void emit(String code, int operand) {
      emit(code, Integer.toString(operand));
}

    /**
     *  Output a line of assembler for an operation (such as invokevirtual, or getstatic) with two arguments.
     * @param code  name of the operation.
     * @param operand   operand used by code.
     * @param extra  second argument.
     *
     *  Example {@code emit("getstatic", "java/lang/System/in", "Ljava/io/InputStream;");}
     *  (though {@link CodeWriter#getStatic(String, String)}  might be a better solution in this case).
     */
    public static void emit(String code, String operand, String extra) {
        emitf(indent+"%s %s %s\n", code, operand, extra);
    }


    /**
     * Load an int literal onto the stack.
     *
     * @param literal  value to load onto the stack
     *
     *  Example  {@code loadConst(-1);}
     */
    public static void loadConst(int literal) {
    emit("ldc", literal);
    }

    /**
     * Load an String literal onto the stack.
     * @param literal string literal to load onto stack.
     *
     *  Note the output strings need to be in quotes.  If the first character of {@code literal} is neither
     *  '\"' nor '\'' quotation marks are added.
     *
     *   Example  {@code loadConst("\"abc\"");} outputs {@code ldc "abc"} as does {@code loadConst("abc");}.
     */
    public static void loadConst(String literal) {
        emit("ldc", literal);
    }

    /**
     * Load an int local variable onto the stack.
     * @param n  offset of local to load.
     *
     *  Example {@code load(5);} produces code to load the 5th local variable onto the stack.
     */
    public static void load(int n) {
    emit("iload", Integer.toString(n));
    }

    /**
     * Load an int value from the stack into a local variable.
     * @param n  offset of local where value will be stored.
     *
     *  Example {@code store(5);} produces code to put the top of stack into the 5th local variable.
     */
    public static void store(int n) {
        emit("istore", Integer.toString(n));
}


    /**
     * Load a static value onto the stack.
     * @param name   variable name.
     * @param type   java type.
     *
     *  Example {@code getstatic("java/lang/System/out", "Ljava/io/PrintStream;"); }
     *  produces {@code getstatic java/lang/System/out, java/io/PrintStream; }
     *
     */
    public static void getStatic(String name, String type) {
    emit("getstatic", name, type);
}

    /**
     * Reverse of {@link CodeWriter#getStatic(String, String)}.
     * @param name  variable name
     * @param type   java type.
     */
    public static void putStatic(String name, String type) {
    emit("putstatic", name, type);
    }

    /**
     * Set a label on the current line.
     * @param addr  a String naming the label.
     *
     * Example   {@code setLabel("here");}
     */
    public static void setLabel(String addr) {
        emitf("%s:\n", addr);
    }

    /**
     * Jump to a label if the value on the top of the stack is 0.
     * @param label  label to jump to.
     *
     *  Example {@code ifFalse("exit"); }
     */
    public static void ifFalse(String label) {
        emit("ifeq", label);
    }

    /**
     * Jump to a label if the value on the top of the stack is not 0.
     * @param label  label to jump to.
     *
     *  Example {@code ifTrue("exit"); }
     */
   public static void ifTrue(String label) {
        emit("ifne", label);
   }


    /**
     * Unconditional jump.
     *
     * @param label  label to jump to.
     *
     *  Example  {@code goto("exit"); }
     */
    public static void jump(String label) {
        emit("goto", label);
    }

    /**
     * Emit code to call a static method.
     * @param methodName java name of method
     *
     */
    public static void call(String methodName) {
            emit("invokestatic", methodName);
    }

    /**
     * Emit code to call a constructor.
     * @param constructorName java name of method
     *
    */
    public static void constructor(String constructorName) {
        emit("invokespecial", constructorName);
    }

    /**
     * Emit code to call a constructor.
     * @param methodName java name of method
     *
     */
   public static void method(String methodName) {
            emit("invokevirtual", methodName);
        }



}
