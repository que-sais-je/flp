/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import sal.text.ErrorStream;
import sal.text.Patterned;
import sal.text.RE;
import sal.text.Scanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.PrintWriter;

import static sal.misc.Fail.failEmpty;
import static sal.small.Token.EOF;
import static sal.text.ErrorStream.errorCount;

/**
 * Created by simon on 28/05/17.
 */
public class Main {


    static private final Scanner<Token> SCAN = new Scanner(EOF, Token.UNMATCHED, Token.IDENTIFIER).whiteSpace(RE.WS, RE.comment("//"));

    static public String currentText() { return SCAN.currentText(); }

    static public Token currentToken() { return SCAN.currentToken(); }

    static public void scan() { SCAN.scan(); }

    static public boolean tokenIn(Token... tokens) {
        return SCAN.tokenIn(SCAN.currentToken(), tokens);
    }

    static public boolean skipToken(Token... tokens) {
        return SCAN.skipToken(tokens);
    }


   /** Check that the current token is as specified - produce an error message if not found.
     *
     * @param tokens symbol to check for.
     */
    static public boolean mustBe(Token... tokens) {
        return SCAN.mustBe(tokens);
    }

    /** Produce a list of expected tokens.
     *
     * @param tokens list of expected tokens.
     *
     *   Unlike {link #mustBe}  wrongToken assumes the current token didn't match.
     *   It doesn't advance to the next token.
     *
     *   It might be used as the default case of a switch on start values;
     */
    public static String expected(Token ... tokens) {
        return SCAN.expected(tokens);
    }



    /////////////////// Make getting file details look prettier //////////////////////

    // recording details used in SMALL program for file name

    private static String programName = null;

    public static void setProgramName(String s) {
        // change first character to uppercase
        programName = s;
    }

    public static String programName() { return programName; }

    private static BufferedReader getInputReader(String fileName) {
        FileReader inputFile = null;
        try {
            inputFile = new FileReader(fileName);
        } catch (Exception e) {
            return null;
        }
        return new BufferedReader(inputFile);
    }

    private static String getOutputFileName() {
        // ParseToTree will have returned the program name via setProgramName
        // in case the program name includes '/' and this is Windows put in
        // OS specific file separator and add .j suffix
        String fileSep = System.getProperty("file.separator");
        String outputName = programName.replace("/", fileSep) + ".j";
        // check for system property (set with -D on command line)
        String outputDir = System.getProperty("outputdir");
        if (outputDir != null)
            outputName = String.format("%s%s%s", outputDir, fileSep, outputName);
        return outputName;
    }

    private static PrintStream getOutputStream(String fileName) {
        PrintStream outputStream = null;
        try {
            outputStream = new PrintStream(fileName);
        } catch (Exception e) {
            return null;
        }
        return outputStream;
    }


    public static void main(String[] args) {

        String[] jasminArg = new String[1];

        BufferedReader inputReader = null;
        int argsLength = args.length;
        if (argsLength == 0) {
            System.err.println("Valid arguments are: [-Doutputfolder ] ( filename )+ ");
            System.exit(1);
        }
        // process each file in turn
        for (String fileName : args) {

            inputReader = getInputReader(fileName);
            if (inputReader == null) {
                System.err.printf("No file called %s\n", fileName);
                continue;   // go onto next file
            }

            // initialise the error count
            errorCount(0);
            // connect the scanner to the input file
            SCAN.input(inputReader);
            // read the first token
            SCAN.scan();

            // parse the program
            Tree<Token> tree = ParseToTree.program();
            if(errorCount() != 0) {
                System.out.printf("%d errors while parsing %s.  Code generation not attempted\n", errorCount(), fileName);
                continue;
            }
            String outputFile = getOutputFileName();
            System.out.printf("Compiling %s to %s\n", fileName, outputFile);
            PrintStream outputStream = getOutputStream(outputFile);
            if (outputStream == null) {
                System.err.printf("Couldn\'t create output file %s : Skipping code generation\n", outputFile);
            } else {
                // set the output stream for code generation
                CodeWriter.output(outputStream);
                CodeGen.writeProgram(tree);// generate code
                outputStream.close();
                if(errorCount() != 0) {
                    System.out.printf("%d errors during code generation\n", errorCount());
                }
                else {
                    System.out.printf("Generated: %s\n", outputFile);
                    jasminArg[0] = outputFile;
                    jasmin.Main.main(jasminArg);
                }
            }
        }
    }
}
