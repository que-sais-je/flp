/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import sal.text.Templater;
import sal.text.ErrorStream;

import java.util.HashMap;
import java.util.Map;

import static sal.small.CodeWriter.*;
import static sal.small.LoopScope.nextLabel;
import static sal.small.Main.programName;
import static sal.small.Token.*;
import static sal.text.ErrorStream.log;

/** Generate Jasmin (Java Assembler) code from a syntax tree.
 *
 * This is the main output program. It walks over the AST.  The method {@link #writeProgram(Tree)}
 *  takes the syntax tree and embeds it in a 'wrapper' - a Jasmin program with 'housekeeping already done so that all {@link #generate(Tree)}
 *  needs to do is deal with each tree node type as it finds it.
 */

public class CodeGen {


    private static SymbolTable allNames = null;

    private static String className;
    private static Tree<Token> treeRoot;

    public static void readInt() {
        call(className+"/read$int()I");
    }

    public static void printOutput(boolean isString) {
        call(String.format("%s/print$it(%s)V", className, isString ? "Ljava.lang.String;" : "I"));
    }

    public static void outputLn() {
        loadConst("\"\\n\"");
        printOutput(true);
    }



    /**
     * Writes (to the PrintStream provided by CodeWriter) a boilerplate Jasmin program in which is embedded the result of
     * generating code from the AST.
     *
     * @param tree  AST which forms the program.
     */
     public static void writeProgram(Tree<Token> tree) {

         Templater tr = new Templater(CodeWriter.output()) {

             public void toString(String s) {
                 CodeGen.insert(s);
             }
         };

         // need by insert (below)
         treeRoot = tree;

         // provide a name for the class
         className = programName();

         // A skeleton Jasmin file is stored in JasminTemplate as an array of strings
         // read each line and output
         for(String aLine : JasminTemplate.theText) tr.render(aLine);

     }

    private static void insert(String key) {
         switch(key) {
            case "CLASSNAME" :  emitf(className);                       return;
            case "CODE":        allNames = new SymbolTable(1);
                                generate(treeRoot);                     return;
            case "LOCALS":      emitf("%d", allNames.locals());  return;
            default:            return;
        }
    }

    private static Map<Token, String> opCode = new HashMap();

    static {
        // default
        opCode.put(PLUS,   "iadd");
        opCode.put(MINUS,  "isub");
        opCode.put(TIMES,  "imul");
        opCode.put(DIVIDE, "idiv");
        opCode.put(MOD,    "imod");
        // these aren't in the 'default list' but are checked
        // for explicitly - this is a place to store there opcodes
        opCode.put(GT,    "if_icmpgt");
        opCode.put(GE,    "if_icmpge");
        opCode.put(LT,    "if_icmplt");
        opCode.put(LE,    "if_icmple");
        opCode.put(EQ,    "if_icmpeq");
        opCode.put(NE,    "if_icmpne");

    }

    /**
     * Generate Jasmin assembler from an AST.
     * @param t  The AST.
     */
    public static void generate(Tree<Token> t) {

        if (t == null) return;
        Token token = t.token();
        switch (token) {
            case NUMBER:
            case STRING:
                loadConst(t.toString());
                break;

            case IDENTIFIER:    // use symbol table to find & generate load for this
            {
                String varName = t.toString();
                int offset = allNames.getToLoad(varName);
                if (offset < 0) {   // has name been declared yet
                    ErrorStream.log("Variable %s is used before it has been declared", varName);
                } else
                    load(offset);
            }
                break;

            case SEMICOLON:
                for(Tree<Token> tst: t.allChildren()) generate(tst);
                break;

            case ASSIGN:
                generate(t.right());
                store(allNames.getToStore(t.left().toString()));
                break;

            case IF: {
                generate(t.left());
                String label = nextLabel("end_if");
                ifFalse(label);
                generate(t.right());
                setLabel(label);
                }
            break;

            case WHILE: {
                LoopScope.begin("continue", "break");
                String continueLabel = LoopScope.label(0);
                String breakLabel = LoopScope.label(1);
                setLabel(continueLabel);
                generate(t.left());
                ifFalse(breakLabel);
                generate(t.right());
                jump(continueLabel);
                setLabel(breakLabel);
                LoopScope.end();
            }
            break;

            case PRINT:
                for(Tree<Token> exp: t.allChildren()) {
                    boolean isString = exp.token() == STRING;
                    generate(exp);
                    printOutput(isString);
                }
                break;

            case READ:
                for(Tree<Token> identifier : t.allChildren()) {
                    readInt();   // code to call scanner to get an integer
                    store(allNames.getToStore(identifier.toString()));
                }
                break;

            case NEGATE:
                generate(t.left());
                emit("ineg");
                break;

            case LE:
            case LT:
            case GE:
            case GT:
            case EQ:
            case NE:
                {
                    /* if the test is false, the code falls through to the next line
                       where 0 (i.e.false) is loaded onto the stack. There is then a jump to the end.
                       The next line is the target when the test was true, -1 (true) is loaded onto the stack
                     */
                    String s = opCode.get(token);
                    generate(t.left());
                    generate(t.right());
                    String ifTrue = nextLabel("true");
                    String ifFalse = nextLabel("false");
                    emit(s, ifTrue);
                    emit("iconst_0");
                    jump(ifFalse);
                    setLabel(ifTrue);
                    emit("iconst_m1");
                    setLabel(ifFalse);
                }
                break;

            case PLUS:
            case MINUS:
            case TIMES:
            case DIVIDE:
            case MOD:
                {
                    generate(t.left());
                    generate(t.right());
                    emit(opCode.get(token));
                }
                break;

            default:            //must be a binary operator +, -, *, /, %
                    ErrorStream.log("Unexpected token in code generation %s", t.toString());
        }
    }

}



