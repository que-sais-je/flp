/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import java.util.EnumSet;

import static sal.small.Tree.*;
import static sal.small.Main.*;
import static sal.small.Token.*;

/**  Reads the source and generates an AST (abstract syntax tree). There is a separate method for each grammar rule.
 * Each method called returns an AST for the statement it has processed.
 * The Syntax rules for each method is given with the method.
 */
public class ParseToTree {


    /**
     * Parse a program.
     *  Grammar rule {@code program         : 'program' name ('/' name )*; statementList }
     *
     * @return AST for complete program.
     */
    public static Tree<Token> program() {
        // first get the program name:
        mustBe(PROGRAM);
        // program names of the form a/b/c are allowed
        StringBuilder sb = new StringBuilder();
        for(;;) {
            sb.append(currentText());
            mustBe(IDENTIFIER);
            if (!skipToken(DIVIDE)) break;
            sb.append("/");
        }
        Main.setProgramName(sb.toString());
        Tree<Token> t = statementList();	// a program consists of a sequence of statements
        mustBe(EOF);
        return t;
    }

    /**
     *  Return a list of parsed statements.
     *  Grammar rule {@code statementList   : (statement)* }

     * @return AST for statementList.
     */
    public static Tree<Token> statementList() {
        Tree<Token> tnew = list(SEMICOLON); // use to indicate a statement list
        Tree<Token> t = statement();
        while(t != null) {
            tnew.addChild(t);
            t = statement();
        }
        return tnew;
    }


    /**
     * Grammar rule {@code statement       : ifStatement | whileStatement | assignStatement | printStatement | readStatement | ';' }
     * @return AST for statement.
     */
    public static Tree<Token> statement() {
        // skip semicolons treated as empty statements
        while(skipToken(SEMICOLON) ) /* do nothing */;

        switch(currentToken()) {
            case IF:            return ifStatement();
            case WHILE:         return whileStatement();
            case PRINT:         return printStatement();
            case IDENTIFIER:	return assignment();
            case READ:          return readStatement();
            default :           return null;
        }
    }

    /**
     * Grammar rule  {@code   ifStatement     : 'if' expression 'then' statementList 'end' }
     * @return AST for ifStatement
     */
    public static Tree<Token> ifStatement() {
        scan(); // skip the 'if' token
        Tree<Token> t  = expression();
        mustBe(THEN);
        t = binary(IF, t, statementList());
        mustBe(END);
        return t;
    }

    /**
     * Grammar rule  {@code   whileStatement     : 'while' expression 'do' statementList 'end' }
     * @return AST for ifStatement
     */
    public static Tree<Token> whileStatement() {
        scan(); // skip the 'while' token
        Tree<Token> t = expression();
        mustBe(DO);
        // variables appearing for the first time in this statementList are local to it
        t = binary(WHILE, t, statementList());
        mustBe(END);
        return t;
    }

    /**
     * Grammar rule {@code   readStatement   : 'read' name (, name)* }
     * @return AST for read statement
     */
    public static Tree<Token> readStatement() {
        scan();  // skip the 'print' token
        Tree<Token> readList = list(READ);
        do {
            String name = currentText();
            mustBe(IDENTIFIER);
            readList.addChild(leaf(IDENTIFIER, name));
        } while (skipToken(COMMA));
        return readList;
    }

    /**
     * Grammar rules {@code printStatement  : 'print' printItem (',' printItem )* }
     * and          {@code printItem       : stringLiteral | expression }
     * @return AST for print statement
     */
    public static Tree<Token> printStatement() {
        scan(); // skip the word 'print'
        Tree<Token> printList = list(PRINT);
        do {
            Token t = currentToken();
            Tree<Token> next = null;
            if (t == STRING) {
                next = leaf(STRING, currentText());
                scan();
            } else {
                next = expression();
            }
            printList.addChild(next);
        } while (skipToken(COMMA));

        return printList;
        }

    /**
     * Grammar rule {@code assignStatement : name '=' expression }
     * @return AST.
     */
    public static Tree<Token> assignment() {
        Tree<Token> t =leaf(IDENTIFIER, currentText());
        scan();
        mustBe(ASSIGN); // skip over the = token
        t = binary(ASSIGN, t, expression());
        //mustBe(SEMICOLON, END);
        return t;
    }

    private static final EnumSet<Token> RELATIONALOPS = EnumSet.of(LE, LT, GE, GT, EQ, NE);

    /**
     * Grammar rule {@code expression      : relopExpression }
     * @return AST.
     */
    public static Tree<Token> expression() {
        return relopExpression();
    }

    // start with lowest priority <, <= etc

    /**
     *  Grammar rule {@code  relopExpression : addExpression [ ('<' | '<=' | '==' | '!=' | '<=' | '<' ) addExpression ] }
     * @return AST.
     */
    public static Tree<Token> relopExpression() {
        Tree<Token> t = addExpression();
        Token tok = currentToken();
        if(RELATIONALOPS.contains(tok)) {
            scan();
            t = binary(tok, t, addExpression());
        }
        return t;

    }

    private static final EnumSet<Token> ADDOPS = EnumSet.of(PLUS, MINUS);



    /**
     * Grammar rule {@code addExpression   : multExpression ( ('+' | '-') multExpression )*}
     * @return AST.
     */
    public static Tree<Token> addExpression() {
        Tree<Token> t = multExpression();
        for(Token tok = currentToken(); ADDOPS.contains(tok); tok = currentToken()) {
            scan();
            t = binary(tok, t, multExpression());
        }
        return t;
    }

    private static final EnumSet<Token> MULTOPS = EnumSet.of(TIMES, DIVIDE, MOD);

    /**
     * Grammar rule {@code multExpression  : term ( ('*' | '/' | '%' ) term )* }
     * @return AST.
     */
    public static Tree<Token> multExpression() {
        Tree<Token> t = term();
        for(Token tok = currentToken(); MULTOPS.contains(tok); tok = currentToken()) {
            scan();
            t = binary(tok, t, term());
        }
        return t;
    }

    /**
     * Grammar rule {@code term            : '(' expression ')' | name | number | '-' term}
     * @return AST.
     */
    public static Tree<Token> term() {
        Token tok = currentToken();
        Tree<Token> t = null;

        switch(tok) {
            case LP:        scan();    // get next token
                            t = expression();
                            mustBe(RP);
                            return t;
            case IDENTIFIER:
            case NUMBER:    t = leaf(tok, currentText());
                            scan();
                            return t;

            case MINUS:     scan();
                            return unary(NEGATE, term());

            default :       mustBe(IDENTIFIER, NUMBER, NEGATE, LP);  // didn't find the start of an expression - there has to be one;

        }
        return t;
    }


}

