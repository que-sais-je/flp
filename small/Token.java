/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import sal.text.RE;
import sal.text.Patterned;

import static sal.text.RE.*;

/**
 * This file contains the definition of the enum Token.
 */
public enum Token implements Patterned {

        EOF,
        UNMATCHED,

        NUMBER(some(in(DIGIT)), "<number>"),
        IDENTIFIER(ALPHA+any(in("A-Za-z0-9_")), "<identifier>"),
        STRING(DQUOTE+any(notIn(DQUOTE)) + DQUOTE, "<string>"),

        PROGRAM("program"),
        IF("if"), THEN("then"), END("end"),

        WHILE("while"), DO("do"),
        PRINT("print"),
        READ("read"),
        ASSIGN(one("="), "="),

        EQ(two("="), "=="), NE("!="),
        GT(thenNot(">", in(">=")), ">"), GE(">="),
        LT(thenNot("<",in("<=")), "<"), LE("<="),

        SHL(two("<"), "<<"),
        SHR(two(">"), ">>"),
        SHRS(three(">"), ">>>"),

        PLUS(RE.PLUS,"+"), MINUS("-"), NEGATE(null,"-"), TIMES("\\*", "*"), DIVIDE("/"), MOD("%"),

        // punctuation ...
        SEMICOLON(";"), COMMA(","), LP("\\(", "("), RP("\\)", ")")

    ;  //////////// end of constants //////////////////////////

        String pattern;     // REGEX used to match token
        String asText;      // text used to display token

        Token(String pattern, String text) {
            this.pattern = pattern;
            this.asText  = text;
        }

        Token(String pattern) {
            this(pattern, true);
        }

        Token(String pattern, boolean isOwnText)
        {  this(pattern, null);
           this.asText = isOwnText ? pattern : this.toString();
        }

        Token() { this(null, false); }


    public String pattern() { return pattern; }

    public String asText()  { return asText; }
}

/*    private final static String STRINGPATTERN = RE.DQUOTE  // start with double quote
            + RE.oneOf(RE.BS+RE.in("btnrf", RE.BS, RE.DQUOTE, RE.SQUOTE), // escaped
            RE.notIn(RE.DQUOTE)         // anything which isn't a quote
    )
            + RE.DQUOTE;


  */