/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.small;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by simon on 29/05/17.
 */


    public class  Tree<T extends Enum<T> > {

    protected T token;

    public Tree() { }

    public Tree(T token) {
        this.token = token;
    }

    public T token() { return this.token; }

    public boolean isLeaf() { return false; }

    public Tree<T> child(int childIndex) { return null; };

    public int children() {
        return 0;
    };

    public List< Tree<T> > allChildren() { return null; }
    // convenient short hand

    public Tree<T> left()   { return child(0);};

    public Tree<T> right()  { return child(1); }

    public void addChild(Tree<T> t) { }

///////////////////////////////////////////////////////////////////////
    public static class Leaf<T extends Enum<T>, V> extends Tree<T> {

        V value;


        public Leaf(T token, V value) {
            super(token);
            this.value = value;

        }

        public V value() {
            return this.value;
        }

        public void setValue(V theValue) {
            this.value = theValue;
        }

        public boolean isLeaf() { return true; }

        public String toString() {
            return value.toString();
        }

}

////////////////////////////////////////////////////////////////////
    public static class Branch<T extends Enum<T>>  extends Tree<T> {

        // accessing nodes in a syntax tree

        protected List< Tree<T> > node;  // list of children

        public Branch(T token, Tree<T> ... children) {
                super(token);
                this.node = new ArrayList(asList(children));
        }

        @Override
        public Tree<T> child(int childIndex) {
            return node.get(childIndex);
        }

        public int children() {
            return node.size();
        };

        public List< Tree<T> > allChildren() { return this.node; }

        @Override
        public void addChild(Tree<T> newChild) {
            this.node.add(newChild);
        }


    }

    public static <T extends Enum<T>, V>  Tree<T>  leaf(T t, V value) {
        return new Leaf<T, V>(t, value);
    }

    public static <T extends Enum<T> >   Tree<T>  unary(T t, Tree<T> tree) {
        return new Branch<T>(t, tree);
    }

    public static  <T extends Enum<T> >  Tree<T>  binary(T token, Tree<T> tree1, Tree<T> tree2) {
        return new Branch<T>(token, tree1, tree2);
    }

    public static <T extends Enum<T> >  Tree<T>  list(T token) {
        return new Branch<T>(token);
    }





}
