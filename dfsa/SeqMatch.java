package sal.dfsa;

import java.util.List;
import static java.util.Arrays.asList;

/**
 * Created by simon on 22/05/17.
 */
public class SeqMatch implements Matcher {

    protected List<Matcher> automata;
    protected int[] resultList;
    protected int   results;  // index to first fail

    public SeqMatch(Matcher... dfa) {
        this.automata = asList(dfa);
        this.resultList = new int[automata.size()+1];
        this.results = 0;
    }

    public boolean match(CharSequence target) { return match(target, 0); }

    public boolean match(CharSequence target, int start) {
        this.results = 0;
        this.resultList[0] = start;
        for(Matcher dfa : automata) {
            if(dfa.match(target, start)) {
                start = dfa.end();
                this.resultList[++this.results] = start;
            } else {
                for(int i = this.results; i < this.automata.size(); i++) this.resultList[i] = start;
                return false;
            }
        }
        return true;
    }

    public int end() { return this.resultList[results]; }
    public int[] result() { return resultList; }
    public int   result(int i) { return resultList[i]; }
    public int   results() { return results; }

}
