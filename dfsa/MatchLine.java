package sal.dfsa;

import static sal.dfsa.DFA.*;

/**
 * Created by simon on 21/05/17.
 */
public class MatchLine {


        public static void main(String[] args)  {

            String test1 = "   ID_1   ID_2  ID3456789###";

            // this uses two DFAs to detect one or two strings on a line
            DFA name = new DFA("FIRST")
                        .link("FIRST", ALPHA, "REST")  // initial char is Alphabetic
                        .link("REST", ALNUM+"_")
                       .endState("REST");

            int start = 0;

            int end   = test1.length();
            while(start < end) {
                // skip white space:
                whiteSpace.match(test1, start);
                int nonSpace = whiteSpace.end();
                name.match(test1, nonSpace);
                int nextSpace = name.end();
                if(nextSpace == nonSpace) {
                    System.out.printf("Nothing found\n");
                    break;
                }
                System.out.printf("Found <%s>\n", test1.substring(nonSpace, nextSpace));
                start = nextSpace;
            }

            // same thing using dfa list
            //Matcher ops = new Matcher(whitespace, name, sink);
            //if(ops.match(test1)) {
            //    int[] r = ops.result();
            //    for (int i : r) System.out.println(i);
            }
            //dfa.trace(true).run("start", "abc123defg");
            //System.out.printf(COPY.toString());
        }
