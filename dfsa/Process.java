/*
 * Copyright (C) 2017 simon
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package sal.dfsa;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 *
 * @author simon
 */
public class Process implements Consumer<Character> {
    
    protected StringBuilder input;
    
    public static final int TOUPPER = 1;
    public static final int TOLOWER = 2;
    public static final int SKIP    = 4;
    
    BiConsumer<Character, StringBuilder> andThen;
    
    int flags;
    
    private Process(int flags, BiConsumer<Character, StringBuilder> bc, StringBuilder sb) {
        this.input = sb;
        this.flags = flags;
        this.andThen = (bc == null) 
                        ? (Character c, StringBuilder sbx) -> { sbx.append(c); }
                        : bc;
    }
    
    public Process(int flags, BiConsumer<Character, StringBuilder> bc) {
        this(flags, bc, new StringBuilder());
    }
    
    public Process(BiConsumer<Character, StringBuilder> bc) {
        this(0, bc); 
    }
    
    public Process(int flags) {
           this(flags, null); 
    }
    
    public Process() {
           this(0, null); 
    }
    
    
    @Override
    public void accept(Character c) {
    
        if((this.flags & SKIP) != 0) return;
        
        // preprocessing ....
        if((this.flags & TOUPPER) != 0) 
            c = Character.toUpperCase(c);
        else if((this.flags & TOLOWER) != 0)
            c = Character.toLowerCase(c);
        
        this.andThen.accept(c, input);
    }

    public void reset() {  input.setLength(0); }
    
    public void reset(int flags) {
        reset(); 
        flags();
    }
        
    public Process flags(int flags) 
    { this.flags = flags;
      return this;
    }
    
    public int flags() { return this.flags; }

    @Override
    public String toString() { return input.toString(); }

            
    
}
