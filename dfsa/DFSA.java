
/**
 *
 * @author simon
 */
/*
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or  (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.dfsa;

import static sal.dfsa.DFA.*;

public class DFSA {


    public static void main(String[] args)  {

        DFA dfa = new DFA("start");
        dfa .link("start",ALPHA)        // ignore non-digits
            .link("start", DEC, "digits")
            .link("digits", DEC)
            .link("digits", not(DEC), "end")
            .link("end", ANY)
            .endState("end");

        dfa.trace(true);
        String test = "abc123defg";
        if(dfa.match("start", test ))
            System.out.println("matched \""+test+"\"");

    }

}
    

