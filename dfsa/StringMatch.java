package sal.dfsa;

/**
 * Created by simon on 22/05/17.
 */
public class StringMatch implements Matcher {

    protected String text;
    protected int textLength;
    protected int   end;
    protected boolean caseBlind;

    public StringMatch(String text, boolean caseBlind) {
        this.text = text;
        this.textLength = text.length();
        this.caseBlind = caseBlind;
    }

    public StringMatch(String text) {
        this(text, false);
    }

    @Override
    public boolean match(CharSequence seq, int start) {
        int ln = seq.length() - start;
        this.end = start;
        if (ln < this.textLength) {
            return false;
        }
        // 'equals' not defined for arbitrary char sequences so
        for (int i = 0; i < this.textLength; ) {
            char here = text.charAt(i),
                    there = seq.charAt(start + i++);
            if (this.caseBlind) {
                here = Character.toLowerCase(here);
                there = Character.toLowerCase(there);
            }
            if (here != there) return false;
        }
        this.end = this.textLength + start;
        return true;
    }


    @Override
    public int end() {
        return this.end;
    }
}
