package sal.dfsa;

/**
 * Created by simon on 22/05/17.
 */
public interface Matcher {

    boolean match(CharSequence seq, int start);
    int end();

    static Matcher str(String s)                { return new StringMatch(s); }
    static Matcher strNoCase(String s)          { return new StringMatch(s, true); }
    static Matcher seqOf(Matcher... m)          { return new SeqMatch(m); }
    static Matcher oneOf(Matcher... m)          { return new AltMatch(m); }
    static Matcher oneOf(String... s)           { return new AltMatch(s); }
    static Matcher any(Matcher m)               { return new RepeatMatch(m, 0); }
    static Matcher some(Matcher m)              { return new RepeatMatch(m, 1); }
    static Matcher maybe(Matcher m)             { return new RepeatMatch(m, 0, 1); }
    static Matcher just(int n, Matcher m)       { return new RepeatMatch(m, n, n); }

}
