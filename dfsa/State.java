package sal.dfsa;

/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;

/**
 * Created by simon on 17/04/17.
 *
 */
public class State extends ArrayList<Link> {

        protected String name;
        protected boolean terminal;

        public State(String name, boolean terminal) {
            this.name = name;
            this.terminal = terminal;
        }


        public State(String name) {
            this(name, false);
        }

        @Override
        public String toString() {
            return name + super.toString();
        }

        public String name() { return name; }

        public void endState(boolean onoff) {
            terminal = onoff;
        }

        public boolean endState() {
            return terminal;
        }


        public State link(String accept, State nextState) {
            this.add(new Link(accept, nextState));
            return this;
        }

        public State link(String accept) {
            this.add(new Link(accept, this));
            return this;
        }

         public Link find(char ch) {
            for(Link t : this) {
                if(t.accepts(ch)) return t;
            }
            return null;
        }

    }

