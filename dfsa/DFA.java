package sal.dfsa;

/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by simon on 17/04/17.
 * DFA realises a simple Deterministic Finite State Automata (DFA)
 */

public class DFA extends HashMap<String, State> implements Matcher {


    protected boolean trace;
    protected int     start;
    protected int     end;
    protected State   startState;

    public DFA(String start) {
        this(start, false);
    }

    public DFA(String start, boolean trace) {
        this.startState = getState(start);
        this.trace = trace;
    }

    /* To do:
     *
     * Add set/get for read/write inc asString, toString etc
     */


    public DFA     trace(boolean onOff) {
        this.trace = onOff;
        return this;
    }
    public boolean  trace() { return this.trace; }

    public int end() { return this.end;  }

    public int start() { return this.start; }


    protected State getState(String stateName) {
        if(this.containsKey(stateName)) return this.get(stateName);
        State s = new State(stateName);
        put(stateName, s);
        return s;
    }

    public boolean match(String current, CharSequence source, int start) {
        return match(getState(current), source, start);
    }

    public boolean match(String current, CharSequence source) {
        return match(getState(current), source, 0);
    }

    public boolean match(CharSequence source, int start) {
        Objects.requireNonNull(this.startState, "No start state specified for DFA");
        return match(this.startState, source, start);
    }

    public boolean match(CharSequence source) {
        return match(source, 0);
    }

    public boolean match(State current, CharSequence source, int start)  {
        // initial settings
        int end = source.length();
        this.start = start;
        while (start < end) {
            char nextCh = source.charAt(start++);

            if(this.trace) System.out.printf("Character  \'%c\' sent to state <%s>\n", nextCh, current.name());
            Link t = current.find(nextCh);
            if (t == null) {
                if (this.trace) System.out.printf("State <%s> does not accept \'%c\'\n", current.name(), nextCh);
                start--; // back track to last accepted character
                break;
            }
            // change state
            State old = current;
            current = t.nextState();
            if (this.trace && (current != old)) System.out.printf("State changed to <%s>\n", current.name());
        }
        boolean success = current.endState();
        this.end = start;
        if (this.trace) System.out.printf("Match %s.\n", success ? "succeeded" : "failed");
        return success;
    }

    // Methods to build transitions

    public DFA  link(String startState, String accept, String endState) {
        getState(startState).link(accept, getState(endState));
        return this;
    }

    public DFA  link(String startState, String accept) {
        State s = getState(startState);
        s.link(accept, s);
        return this;
    }
    
    public DFA endState(String state) {
        this.getState(state).endState(true);
        return this;
    }

    public DFA startState(String state) {
        this.startState = getState(state);
        return this;
    }

    public State startState() {
        return this.startState;
    }

    /* Useful statics */
    
    public final static String WS = " \t";
    public final static String VS = "\n\r\f";
    public final static String ANY = "\u0000-\u007f";
    public final static String DEC = "0-9";
    public final static String OCT = "0-7";
    public final static String BIN = "0-1";
    public final static String HEX = "0-9a-fA-F";
    public final static String SQ =  "\'";
    public final static String DQ = "\"";
    public final static String LOWER = "a-z";
    public final static String UPPER = "A-Z";        //  An upper-case alphabetic character:[A-Z]
    public final static String ALPHA = "A-Za-z";     //  	An alphabetic character
    public final static String ALNUM = "A-Za-z0-9";  // 	An alphanumeric character
    public final static String CARET = "^-^";

    public static String not(String s) { return ANY+'^'+s; }
    public static String chars(String s1, String s2) { return s1+'^'+s2; }
    
    public final static DFA whiteSpace  = new DFA("SkipWhiteSpace").endState("SkipWhiteSpace").link("SkipWhiteSpace", WS);

    public final static DFA sink   = new DFA("Sink").endState("Sink").link("Sink", ANY);

    public static DFA name(String init, String rest) {
        return new DFA("NAME")
                .link("NAME", init, "REST")
                .link("REST", rest)
                .endState("NAME").endState("REST");
    }

    public final static DFA name   =  DFA.name(ALPHA, ALNUM+"_");

    public static DFA some(String str) { return new DFA("SOME").endState("MORE").link("MORE", str); }

    public static DFA maybe(String str) { return new DFA("MAYBE").endState("MAYBE").endState("SOME").link("SOME", str); }

    public static DFA any(String str) { return new DFA("SOME").endState("SOME").link("SOME", str); }

    public static DFA whitespace = any(WS);

    public static DFA seq(String str) {
       int len = str.length();
        String last = "sequence";
        DFA dfa = new DFA(last);
        int i = 0;
        while(i < len) {
            char c = str.charAt(i++);
            String find = Character.toString(c);
            dfa.link(last, find + "-" + find, find);
            last = find;
        }
        return dfa.endState(last);
    }

}
