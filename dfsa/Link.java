package sal.dfsa;

/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import java.util.BitSet;
import java.util.Objects;

/**
 * Created by simon on 18/04/17.
 */
public class Link extends BitSet{

    protected State nextState;

    public Link(String accept, State state) {
        super(256);
        nextState   = state;
        makePairList(accept);
    }

    private void makePairList(String str) {
        Objects.requireNonNull(str,"Transition has no accepting values");
        int strLen = str.length();
        if(strLen == 0) throw new IllegalArgumentException("Transition has no accepting values");
        int last = strLen - 2;          // first point at which a range (e.g. a-z) isn't possible
        // check if string starts with ^ (but not ^-^) i.e negating entries
        boolean accepting = (str.charAt(0) != '^') || str.startsWith("^-^");
        if(! accepting)  this.set(0, 256, accepting); // assume everything
        for (int i = 0; i < strLen; i++) {
            char nextCh = str.charAt(i);
            int iChar   = (int)nextCh;
            // first check for a range pair, e.g. a-z
            if (i < last && str.charAt(i + 1) == '-') {
                // always a range since there is another next char after the '-'
                i += 2;
                this.set(iChar, (int)(str.charAt(i)) + 1, accepting);
            }
            else if (accepting && nextCh == '^') {
                // change the flag so we don't do this next time we see a '^'
                accepting = false;
            } else
                this.set(iChar, accepting);
        }

        //System.out.printf("\"%s\" -> \"%s\", \"%s\"\n",str, this.accept, this.reject);
    }

    public boolean accepts(char c) {
        return this.get((int)c);
    }

    public State nextState() {
        return nextState;
    }
    public void nextState(State s) {
        nextState = s;
    }



}    


