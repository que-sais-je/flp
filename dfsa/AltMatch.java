/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.dfsa;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

/**
 * Created by simon on 22/05/17.
 */
public class AltMatch implements Matcher {

    protected List<Matcher> alternatives;
    protected int end;

    public AltMatch(Matcher... alts) {
        alternatives = asList(alts);
    }

    public AltMatch(String... alts) {
        alternatives = asList(alts).stream().map(x-> new StringMatch(x)).collect(Collectors.toList());
    }


    @Override
    public boolean match(CharSequence seq, int start) {
        for(Matcher m : alternatives)
            if(m.match(seq, start)) {
                this.end = m.end();
                return true;
            }
        this.end = start;
        return false;
    }

    @Override
    public int end() {
        return this.end;
    }
}
