package sal.dfsa;

/**
 * Created by simon on 22/05/17.
 */
public class RepeatMatch implements Matcher {

    /**
     * Value which can be used to represent unlimited repetitions.
     */
    protected int low, high;
    protected int end;
    Matcher pattern;

    /**
     * Repeat a given sequence a number of times. MANY can be
     * used to indicate 'indefinite' repetition.
     * @param low The smallest number of iterations allowed.
     * @param high The maximum number of iterations allowed
     * @param m the pattern.
     */

    public RepeatMatch(Matcher m, int low, int high) {
        this.high = high;
        this.low = low;
        this.pattern = m;
    }

    public RepeatMatch(Matcher m, int low) {
        this(m, low, Integer.MAX_VALUE);
    }


    @Override
    public int end() {
        return this.end;
    }

      @Override
      public boolean match(CharSequence seq, int start) {
        int index = start;
        for(int i = 1; i <= high; i++ )
          if(pattern.match(seq, index))
              index = pattern.end();
          else if(i >= this.low)
              break;
          else {
              this.end = start;
              return false;
        }
        this.end = index;
        return true;
    }

}
