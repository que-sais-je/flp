/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.text;

/**
 * Created by simon on 29/05/17.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static sal.misc.Fail.failEmpty;

/**
 * Created by simon on 26/05/17.
 */
public class Scanner<T extends Enum<T> & Patterned > {

    // ********************* TOKENS ******************************

    /** Predefined token: no match in input.  */
    protected T UNMATCHED;

    /** Predefined Token: end of input.  */
    protected T EOF;

    /** Token representing an identifier,  by default **/
    protected T identifier;

    protected T currentToken;

    private Class<T> enumClass;

    // ********************* PATTERNS ****************************

    /** RE Expression matcher compiled from token patterns. **/
    protected Matcher tokenMatcher;

    /** Map for fixed text tokens (i.e. 'if', 'then') */
    protected Map<String, T> words;

    protected Map<String, T> patternNames;

    /** RE Expression matcher compiled from token patterns. **/
    protected Matcher spaceMatcher;


    protected int lineNumber;

    // ********************** INPUT BUFFER **************************

    /** A view of the current input buffer - from the end of the last found
     * token to the end of the buffer.
     **/
    protected CharView buffer;

    protected BufferedReader reader;

    protected String readerName;

    // *********************** CURRENT TOKEN ***********************

    /** A view of the last token found.  Invalidated as soon as the next
     call to scan is made**/
    protected CharView   text;

    public Scanner(T eof, T unmatched) {
        this(eof, unmatched, null);
    }

    public Scanner(T eof, T unmatched, T identifier)
    {
        // 1. ******************* TOKENS ******************************

        this.EOF = eof;
        this.identifier = identifier;
        this.currentToken = this.UNMATCHED = unmatched;

            /* Build an RE match to describe all tokens */
        String pattern = null;						// to hold all patterns as created

            /* first create an identifier pattern - a java name by default */
        String p = null;
        if(identifier != null) {
            p = identifier.pattern();    // identifier has a pattern
            if (p == null) p = RE.JAVANAME;     // or use Java Id as default
        } else {
            // a pattern which always fails!
            p = "(?!)";
        }

        // create a matcher to identify keywords
        Matcher identifierMatcher =  Pattern.compile(p).matcher("");

        this.words = new HashMap<>();				// map: keyword text -> token

        this.patternNames = new HashMap<>();

        // build a pattern to match everything which has a defined pattern which doesn't match identifier
        this.enumClass = eof.getDeclaringClass();

        // if identifier is defined, any token with a pattern which would match identifier is a keyword
        for(T t : this.enumClass.getEnumConstants()) {
            String tokenPattern = t.pattern();
            if(tokenPattern == null) continue;  // not detected in pattern matcher

            // check if pattern matches identifier
            identifierMatcher.reset(tokenPattern);
            if(identifierMatcher.matches() ) {
                // keyword found
                this.words.put(tokenPattern, t);
            } else {
                // add this as a named group
                String tokenName = t.toString();
                String matchTokenGroup = String.format("(?<%s>%s)", tokenName, tokenPattern);
                pattern = (pattern == null) ? matchTokenGroup : (pattern + "|" + matchTokenGroup);
                this.patternNames.put(tokenName, t);
            }
        }

        // create a matcher from the patterns
        this.tokenMatcher   = Pattern.compile(pattern).matcher("");

        // initialise defaulter matcher for white space
        this.whiteSpace(RE.WS);

        // 3. ********************** INPUT BUFFER **************************

        /** A view of the current input buffer - from the end of the last found
         * token to the end of the buffer.
         **/

        this.buffer = new CharView();

        this.lineNumber = 0;

        this.readerName = null;

        this.text   = new CharView();
    }

    // override whitespace defaults
    public Scanner<T> whiteSpace(String... alts) {
        this.spaceMatcher = Pattern.compile(RE.any(alts)).matcher("");
        return this;
    }




    //  ************** TOKENISING ********************

    /** Initialise Tokeniser ready for new input.
     */

    public Scanner<T> inputName(String input) {
        this.readerName = input;
        ErrorStream.errorSource(input);
        return this;
    }

    public String inputName() {
        return this.readerName;
    }

    public Scanner<T> input(BufferedReader reader) {
        this.text.set(0, 0);
        this.reader 	= reader;	// forget previous input
        this.readerName = null;
        ErrorStream.errorSource(null);
        this.lineNumber = 0;
        this.currentToken = UNMATCHED;
        return this;
    }

    public Scanner<T> input(String source) {
        return input(new BufferedReader(new StringReader(source)));
    }

    public Scanner<T> input(Reader read) {
        return input(new BufferedReader(read));
    }


    /** Deal with IOException.
     *
     * Sub classes should override this.
     */

    public void exception(IOException ioerr)
    {
        ErrorStream.log(ioerr);
    }


    /** Output a message when unrecognisable text is found.
     *
     */
    public void error() {
        String errStr = this.text.toString();
        char errch = errStr.charAt(0);
        if( errch < ' ' || errch > '~' )
            errStr = String.format("\\u%04x", (int)errch);
        ErrorStream.log(this.lineNumber, "Unexpected character \'"+errStr+"\'");
    }

    public  int lineNumber() { return this.lineNumber; }

    public  int columnNumber() { return this.text.getBeginIndex(); }

    private boolean fillBuffer() {

        // Empty buffer so try to read another line
        String newBuffer = null;
        try {
            newBuffer = reader.readLine();
        } catch (IOException ioerr) {
            // exception can be over ridden by subclass to change behaviour
            exception(ioerr);
            text.set("<I/O UNMATCHED>");
            this.currentToken = EOF;
            return false;
        }

        // read line returns null at eof
        if (newBuffer == null) {
            this.currentToken = EOF;
            text.set("<EOF>");
            return false;
        }

        // otherwise a line has been read
        // count line
        this.lineNumber++;
        // set text buffer to what was read (with \n)
        this.buffer.set(newBuffer+"\n");
        // set last text read to empty
        this.text.set(newBuffer, 0, 0);
        this.currentToken = UNMATCHED;  // set to imply: continue to look for a match
        return true;
    }



    public T scan() {

        Matcher wsMatcher = this.spaceMatcher;
        Matcher tokenMatcher = this.tokenMatcher;

        // First ensure there is some data
            /* Should only be EOF when input is complete.
               Subsequent calls will continue to return EOF.
             */
        while (currentToken != EOF) {

            // first check if buffer is empty
            if ((this.buffer.length() == 0) && ! fillBuffer()) return this.currentToken;

            // there must be at least one character in the buffer
            // check for leading ws

            // now look for a real token
            CharView buff = this.buffer;

            wsMatcher.reset(buff);
            wsMatcher.lookingAt(); // never fails!
            // now step over the white space
            buff.set(buff.getBeginIndex() + wsMatcher.end());
            if(buff.length() == 0) continue; // whole line consumed

            // set the matcher to refer to the current buffer.
            tokenMatcher.reset(buff);

            // by default assume nothing matches
            int textLength = 1;
            this.currentToken = UNMATCHED;
            // check for a matching symbol at the start of input.
            if (tokenMatcher.lookingAt()) {
                // extract data about match
                for (Map.Entry<String, T> pattern : this.patternNames.entrySet()) {
                    //System.out.printf("Buffer \"<<%s>>\" Trying group %s\n", buff, patternName);
                    String grp = tokenMatcher.group(pattern.getKey());
                    if (grp != null) {
                        textLength = grp.length();
                        //this.currentToken = this.nameToToken.get(patternName);
                        this.currentToken = pattern.getValue();
                        //System.out.printf("Buffer \"<<%s>>\" matching \"%s\"\n", buff, grp);
                        break;
                    }
                }
            }

            // see what's happening:
            //System.out.printf("Token %s: @%d length %d\n", this.currentToken, buff.getBeginIndex(), textLength);
            int start = buff.getBeginIndex();
            int end = start + textLength;
            this.text.set(start, end);
            // move buffer point forward
            buff.set(end);
            if (this.currentToken == UNMATCHED) this.error();
            // check for a keyword
            if (this.currentToken == this.identifier)
                this.currentToken = this.words.getOrDefault(this.text.toString(), this.identifier);
            break;
        }
        return this.currentToken;
    }


    /** Check that the current token is as expected.
     *
     * @param tokens symbol to ignore if found: good for lazy languages
     * where, maybe, the 'do' in {@code while ... do} is optional.
     * @return  true if the expected token was fount, false otherwise.
     */
    public boolean tokenIn(T elem, T... tokens) {
        for(T t : tokens) {
            if(t == elem) return true;
        }
        return false;
    }

    public boolean tokenIn(T... tokens) {
        return this.tokenIn(currentToken, tokens);
    }

    public boolean skipToken(T... tokens) {
        boolean check = this.tokenIn(currentToken, tokens);
        if(check) scan();
        return check;
    }
    /** Check that the current token is as specified - produce an error message if not found.
     *
     * @param tokens symbol to check for.
     */
     public boolean mustBe(T... tokens) {
        boolean checkOK = this.tokenIn(tokens);
        if(checkOK) {
            scan();
        } else {
            ErrorStream.log(this.lineNumber, this.text, "Found %s when expecting %s\n", this.currentText(), expected(tokens));
        }
        return checkOK;
    }

    /** Produce a list of expected tokens.
     *
     * @param tokens list of expected tokens.
     *
     *   Unlike {link #errorIfNot}  wrongToken assumes the current token didn't match.
     *   It doesn't advance to the next token.
     *
     *   It might be used as the default case of a switch on start values;
    */
    public String expected(T... tokens) {
        failEmpty(tokens);
        StringBuilder sb = new StringBuilder();
        if(tokens.length > 1) {
            sb.append("one of");
        }
        for(T t : tokens) sb.append(" ").append(t.asText());
        return sb.toString();
    }

    public T currentToken() { return this.currentToken; }

    public CharView buffer() {
        return this.buffer;
    }


    public String currentText() {
        return this.text.toString();
    }

}
