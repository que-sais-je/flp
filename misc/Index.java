/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sal.misc;

import java.util.List;

/**
 * Created by simon on 22/05/17.
 */
public class Index {

        /**
         * Pythonize an index. If the index is -ve treat it as relative to end
         * of sequence. I.e. -n is index to length() - n.
         *
         * @param   index Given index
         * @param   max len
         * @return  Checked and adjusted index
         * @throws  IndexOutOfBoundsException if index doesn't correspond to a valid
         */
        public static int index(int index, int len) {
            if(index >= len) {
                throw new IndexOutOfBoundsException(String.format("Expected index %1$d < %2$d", index, len));
            } else if(index < 0) {
                index += len;
                if(index < 0) {
                    throw new IndexOutOfBoundsException(String.format("Expected index %1$d >= %2$d", index - len, -len));
                }
            }
            return index;
        }

        static public <T> int index(List<T> al, int indx) {
            return index(indx, al.size());
        }

        static public int index(CharSequence str, int indx) {
            return index(indx, str.length());
        }

        static public <T> T at(List<T> al, int indx) {
            return al.get(index(al, indx));
        }

        static public <T> void at(List<T> al, int indx, T val) {
            al.set(index(al, indx), val);
        }

        static public char at(CharSequence str, int indx) {
            return str.charAt(index(indx, str.length()));
        }

    }

